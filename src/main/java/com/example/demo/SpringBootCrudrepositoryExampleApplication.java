package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.entity.Student;
import com.example.repository.StudentRepository;

@SpringBootApplication
@EntityScan("com.example.entity")
@EnableJpaRepositories("com.example.repository")
public class SpringBootCrudrepositoryExampleApplication implements CommandLineRunner{
	
	@Autowired
    StudentRepository studentRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCrudrepositoryExampleApplication.class, args);
	}

	@Override
    public void run(String... args) throws Exception
    {
        System.out.println("***** All Students *******");
        for(Student st : studentRepository.findAll())
        {
            System.out.println(st);
        }
        
        System.out.println("***** Student with Age less than 33 *******");
        for(Student st : studentRepository.findByAgeLessThanEqual(33))
        {
            System.out.println(st);
        }
    }
}
