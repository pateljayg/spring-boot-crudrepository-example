package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.entity.Student;

@RepositoryRestResource
public interface StudentRepository extends CrudRepository<Student, Long> 
{
    public List<Student> findById(long id);

    //@Query("select s from Student s where s.age <= ?")
    public List<Student> findByAgeLessThanEqual (long age);
}
